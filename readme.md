# Usage

* Install the module
* Create a field
* Create a js file for this field like this:

```js
$(document).on('afterInit.rht', '.handsontable', function(e, hot) {
  // make sure the settings are applied to the correct field
  if(e.target.id != 'handson_Inputfield_tmp') return;
  
  var colheaders = [];
  for(var i=1; i<=12; i++) colheaders.push(i);
  var rowHeaders = ['four', 'five', 'six'];

  // update the handsontable field
  hot.updateSettings({
    colHeaders: colheaders,
    minCols: colheaders.length,
    maxCols: colheaders.length,
    rowHeaders: rowHeaders,
    minRows: rowHeaders.length,
    maxRows: rowHeaders.length,
  });
});

$(document).on('afterInit.rht', '.handsontable', function(e, hot) {
  // make sure the settings are applied to the correct field
  if(e.target.id != 'handson_Inputfield_addTrainings') return;
  
  var colheaders = [];
  for(var i=1; i<=12; i++) colheaders.push(i);
  var rowHeaders = ['one', 'two', 'three'];

  // update the handsontable field
  hot.updateSettings({
    colHeaders: colheaders,
    minCols: colheaders.length,
    maxCols: colheaders.length,
    rowHeaders: rowHeaders,
    minRows: rowHeaders.length,
    maxRows: rowHeaders.length,
  });
});
```

![screenshot](https://i.imgur.com/jFp7nxV.png)
