/**
 * trigger pw inputfield toggles and tab changes
 */
$(document).on('opened wiretabclick', function(e) {
    $hot = $(e.target).find('.handsontable');

    // redraw all handsontables inside the toggled field
    $.each($hot, function() {
        var hot = $(this).handsontable('getInstance');
        if(hot) hot.render();
    });
});
